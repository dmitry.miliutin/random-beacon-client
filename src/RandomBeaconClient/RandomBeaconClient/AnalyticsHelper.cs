﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RandomBeaconClient
{
    /// <summary>
    /// Extensions for local bussines tasks
    /// </summary>
    public static class AnalyticsHelper
    {
        /// <summary>
        /// Count symbols in string
        /// </summary>
        /// <param name="str">Input string.</param>
        /// <returns>Collection with <see cref="Item"/> that contains info about symbol and it count into string.</returns>
        public static List<Item> CountSymbols(this string str)
        {
            if (str is null)
                throw new ArgumentNullException("str");

            return str.GroupBy(x => x)
                .Select(x => new Item(x.Key, x.Count()))
                .ToList();
        }
    }
}
