﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;

namespace RandomBeaconClient
{
    /// <summary>
    /// Contains logic for relation between GUI and beacon model
    /// </summary>
    public class BeaconViewModel : INotifyPropertyChanged
    {
        private readonly Beacon beacon;

        public event PropertyChangedEventHandler PropertyChanged;

        #region Public Properties
        public string TimeStamp => beacon.TimeStamp;

        public string OutputValue => beacon.CurrentModel?.OutputValue ?? "Error occurred.";

        public DateTime SelectedDate
        {
            get => beacon.Date;

            set
            {
                beacon.Date = value;
                OnPropertyChanged("TimeStamp");
            }
        }

        public ObservableCollection<Item> Grid { get; set; }
        #endregion

        /// <summary>
        /// Creates view model, init default values
        /// </summary>
        public BeaconViewModel()
        {
            var defaultDate = new DateTime(2018, 12, 26, 10, 10, 01);
            beacon = new Beacon(defaultDate);
            RunButton();
        }

        /// <summary>
        /// Calls Beacon API, reloads output value and item`s table
        /// </summary>
        public void RunButton()
        {
            if (!beacon.UpdateBeacon(out string mess))
                MessageBox.Show(mess);
            else
                UpdateGrid();
        }

        private void UpdateGrid()
        {
            Grid = new ObservableCollection<Item>(beacon.Items);
            OnPropertyChanged("Grid");
        }

        private void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }

    }
}
