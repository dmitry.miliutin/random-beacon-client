﻿using System.Xml.Serialization;

namespace RandomBeaconClient
{
    /// <summary>
    /// Dto model, returned from beacon rest api
    /// </summary>
    [XmlRoot(ElementName = "record", Namespace = "http://beacon.nist.gov/record/0.1/")]
    public class BeaconResponseModel
    {
        [XmlElement(ElementName = "version", Namespace = "http://beacon.nist.gov/record/0.1/")]
        public string Version { get; set; }
        [XmlElement(ElementName = "frequency", Namespace = "http://beacon.nist.gov/record/0.1/")]
        public string Frequency { get; set; }
        [XmlElement(ElementName = "timeStamp", Namespace = "http://beacon.nist.gov/record/0.1/")]
        public string TimeStamp { get; set; }
        [XmlElement(ElementName = "seedValue", Namespace = "http://beacon.nist.gov/record/0.1/")]
        public string SeedValue { get; set; }
        [XmlElement(ElementName = "previousOutputValue", Namespace = "http://beacon.nist.gov/record/0.1/")]
        public string PreviousOutputValue { get; set; }
        [XmlElement(ElementName = "signatureValue", Namespace = "http://beacon.nist.gov/record/0.1/")]
        public string SignatureValue { get; set; }
        [XmlElement(ElementName = "outputValue", Namespace = "http://beacon.nist.gov/record/0.1/")]
        public string OutputValue { get; set; }
        [XmlElement(ElementName = "statusCode", Namespace = "http://beacon.nist.gov/record/0.1/")]
        public string StatusCode { get; set; }
        [XmlAttribute(AttributeName = "xmlns")]
        public string Xmlns { get; set; }
    }
}
