﻿using System.Windows;

namespace RandomBeaconClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new BeaconViewModel();
        }

        public void RunButtonClick(object sender, RoutedEventArgs e)
        {
            (DataContext as BeaconViewModel).RunButton();
        }
    }
}
