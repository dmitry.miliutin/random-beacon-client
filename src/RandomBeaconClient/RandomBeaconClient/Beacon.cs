﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace RandomBeaconClient
{
    /// <summary>
    /// Bussines model with Beacon logic
    /// </summary>
    public class Beacon
    {
        private static readonly string _uri = "https://beacon.nist.gov/rest/record/";
        private static readonly string _errorParse = "Could nor parse response.\n";
        private static readonly string _errorHttp = "Error since sending request.\n";


        private DateTime _date;

        private BeaconResponseModel _currentModel;

        private readonly XmlSerializer _xmlSerializer = new XmlSerializer(typeof(BeaconResponseModel));

        public DateTime Date
        {
            get => _date;
            set
            {
                _date = value;
                TimeStamp = this.GetTimeStamp(value);
            }
        }

        public BeaconResponseModel CurrentModel
        {
            get => _currentModel;
            set
            {
                _currentModel = value;
                SetItems(value);
            }
        }

        public IEnumerable<Item> Items { get; set; }

        public string TimeStamp { get; private set; }

        public Beacon(DateTime date)
        {
            Date = date;
        }

        public bool UpdateBeacon(out string message)
        {
            if (TrySend(out WebResponse response))
            {
                using (response)
                    return TryDeserializeResponse(response, out message);
            }
            else
            {
                using (response)
                    message = _errorHttp + GetError(response);

                return false;
            }
        }

        private string GetError(WebResponse response)
        {
            using (var stream = response.GetResponseStream())
            using (var reader = new StreamReader(stream))
                return reader.ReadToEnd();
        }

        private bool TryDeserializeResponse(WebResponse response, out string message)
        {
            using (var stream = response.GetResponseStream())
            {
                try
                {
                    CurrentModel = (BeaconResponseModel)_xmlSerializer.Deserialize(stream);
                }
                catch(SerializationException ex)
                {
                    message = _errorParse + ex.Message;
                    return false;
                }
            }

            if (CurrentModel is null || CurrentModel.OutputValue is null)
            {
                message = _errorParse;
                return false;
            }

            message = string.Empty;
            return true;
        }

        private bool TrySend(out WebResponse response)
        {
            try
            {
                var request = BuildRequest();
                response = request.GetResponse();
                return true;
            }
            catch (WebException ex)
            {
                response = ex.Response;
                return false;
            }
        }

        private WebRequest BuildRequest()
        {
            var request = WebRequest.Create(_uri + this.TimeStamp);
            request.Method = "GET";
            return request;
        }

        private void SetItems(BeaconResponseModel model)
        {
            this.Items = model.OutputValue.CountSymbols();
        }

        private string GetTimeStamp(DateTime date)
        {
            return new DateTimeOffset(date)
                .ToUnixTimeSeconds()
                .ToString();
        }
    }
}
