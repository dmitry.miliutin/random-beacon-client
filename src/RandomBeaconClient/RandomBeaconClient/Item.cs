﻿namespace RandomBeaconClient
{
    public class Item
    {
        public Item() { }

        public Item(char symbol)
        {
            Symbol = symbol;
        }

        public Item(char symbol, int count) : this(symbol)
        {
            Count = count;
        }
        public char Symbol { get; set; }
        public int Count { get; set; }
    }
}
